{
  const {
    html,
  } = Polymer;
  /**
    `<results-cells>` Description.

    Example:

    ```html
    <results-cells></results-cells>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --results-cells | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class ResultsCells extends Polymer.Element {

    static get is() {
      return 'results-cells';
    }

    static get properties() {
      return {
        results: {
          type: Array,
          value: function() {
            return [];
          }
        }
      };
    }
  }

  customElements.define(ResultsCells.is, ResultsCells);
}